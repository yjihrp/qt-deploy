import os
import shutil


def file_name(path, include_ext=True):
    if path is None:
        return ''

    if include_ext:
        name = os.path.basename(path)
    else:
        name = os.path.basename(os.path.splitext(path)[0])
    return name


def ls_dir(path: str):
    if not os.path.exists(path):
        return []

    if not os.path.isdir(path):
        return []

    files = []
    for root, dirs, items in os.walk(path):
        for item in items:
            files.append(os.path.join(root, item))

    return files


def make_dir(path: str):
    if not isinstance(path, str):
        return False

    if path == '':
        return False

    if os.path.exists(path):
        return True

    try:
        os.makedirs(path)
        return os.path.exists(path)
    except Exception as e:
        print('创建文件夹失败,path={0}'.format(path), e)
        return False


def remove_file(path: str):
    if not isinstance(path, str):
        return False

    if path == '':
        return False

    if not os.path.exists(path):
        return True

    if not os.path.isfile(path):
        return False

    try:
        os.remove(path)
        return not os.path.exists(path)
    except Exception as e:
        print('删除文件失败,path={0}'.format(path), e)
        return False


def remove_dir(path: str):
    if not isinstance(path, str):
        return False

    if path == '':
        return True

    if not os.path.exists(path):
        return True

    if not os.path.isdir(path):
        return False

    try:
        shutil.rmtree(path)
        return not os.path.exists(path)
    except Exception as e:
        print('删除文件夹失败,path={0}'.format(path), e)
        return False


def copy_file(src: str, dest: str):
    if not isinstance(src, str):
        return False
    if src == '':
        return False
    if not os.path.exists(src):
        return False

    if not isinstance(dest, str):
        return False
    if dest == '':
        return False

    if not os.path.isfile(src):
        return False

    try:
        if os.path.exists(dest):
            os.remove(dest)

        path = os.path.dirname(dest)
        if not os.path.exists(path):
            os.makedirs(path)

        shutil.copy(src, dest)
        return os.path.exists(dest)
    except Exception as e:
        print('复制文件失败,path={0}'.format(src), e)
        return False


def copy_file_dir(files: [str, list], path: str, clear=True):
    if not isinstance(files, (str, list)):
        return False
    if isinstance(files, str):
        if files == '':
            return False
        files = [files]
    if isinstance(files, list) and len(files) == 0:
        return False

    try:
        if clear and os.path.exists(path):
            if os.path.isfile(path):
                os.remove(path)
            if os.path.isdir(path):
                shutil.rmtree(path)

        if not os.path.exists(path):
            os.makedirs(path)

        for item in files:
            name = file_name(item)
            dest = os.path.join(path, name)
            if not copy_file(item, dest):
                return False
        return True
    except Exception:
        return False


def copy_dir(src: str, dest: str, clear=True):
    if not isinstance(src, str):
        return False
    if src == '':
        return False

    if not os.path.exists(src):
        return False

    if not os.path.isdir(src):
        return False

    if not isinstance(dest, str):
        return False

    try:
        if clear and os.path.exists(dest):
            if os.path.isfile(dest):
                os.remove(dest)
            if os.path.isdir(dest):
                shutil.rmtree(dest)

        shutil.copytree(src, dest)
        return os.path.exists(dest)
    except Exception as e:
        print('复制文件夹失败,path={0}'.format(src), e)
        return False
