def convert_bool(val):
    if val is None:
        return False
    if isinstance(val, bool):
        return val
    elif isinstance(val, str):
        val = val.lower()
        if val in ('true', 'false', '1', '0'):
            if val in ('true', '1'):
                return True
            else:
                return False
        else:
            return False
    elif isinstance(val, int):
        if val in (1, 0):
            if val == 1:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def convert_numeric(val):
    from .regular import is_number, is_float

    result = 0
    if is_number(val, True):
        result = int(val)
    elif is_float(val):
        result = float(val)
    return result
